grepmail (5.3111-4) unstable; urgency=medium

  * modified debian/rules
    + overrided dh_auto_clean in order to use 'make clean' rather
      than 'make distclean'
    + added some more files to clear upon launching dh_auto_clean
    + Closes: #1049629

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 24 Sep 2023 12:40:49 +0200

grepmail (5.3111-3) unstable; urgency=medium

  * upload to unstable

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 11 Jun 2023 12:14:07 +0200

grepmail (5.3111-2) experimental; urgency=medium

  * creates the directory $(HOME)/.cache if it does not exist: so
    autopkgtest can succeed in a chroot.

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 02 May 2023 19:51:39 +0200

grepmail (5.3111-1) experimental; urgency=medium

  * New upstream version 5.3111
  * added myself as maintainer: Closes: #483247
  * bumped Standards-Version: 4.6.2
  * removed the patch library-warnings.patch which was adopted upstream
  * created a new patch to comply with freedesktop standard.
    Closes: #807595
  * added build-dependencies on libfile-homedir-perl, libtest-compile-perl,
    libfile-slurper-perl, libuniversal-require-perl
  * removed inc/Scalar/Util.pm from d/copyright's mentions as this file
    is no longer in the upstream source, and removed texts of licenses
    (Artistic, GPL-1)

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 02 May 2023 18:07:07 +0200

grepmail (5.3104-5) unstable; urgency=medium

  * QA upload.
  * debian/control: bumped Standards-Version to 4.6.1.
  * debian/copyright: updated rights in packaging block.
  * debian/tests/*: created to provide some CI tests.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 31 Jul 2022 21:59:51 -0300

grepmail (5.3104-4) unstable; urgency=medium

  * QA upload.
  * debian/tests/control: removed because the current CI test was useless.
    Thanks to Paul Gevers <elbrus@debian.org>. (Closes: #998358)

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 10 Nov 2021 23:37:30 -0300

grepmail (5.3104-3) unstable; urgency=medium

  * QA upload.

  [ Guilherme de Paula Xavier Segundo ]
  * debian/control:
    - Add Rules-Requires-Root field.
    - Bump debhelper-compat to 13.
  * debian/salsa-ci.yml: Add to provide CI tests for Salsa.

  [ Marcos Talau ]
  * debian/control:
    - Bump Standards-Version to 4.6.0.1.
    - Update Homepage.
  * debian/copyright:
    - Create stanzas for Artistic and GPL-1+ licenses.
    - Rewrite GPL-2 stanza.
    - Update packaging copyright information.
    - Update Source field.
  * debian/tests/control: Create autopkgtest.

 -- Marcos Talau <marcos@talau.info>  Sun, 31 Oct 2021 13:15:38 -0300

grepmail (5.3104-2) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 20 Aug 2021 13:35:10 +0100

grepmail (5.3104-1) unstable; urgency=low

  * QA upload.
  * New upstream release.
    - LZMA support (Closes: #517109)
    - fix bug with '\/' in complex search patterns (Closes: #432083)
  * Drop patches applied upstream.
  * debian/control:
    + wrap-and-sort
    + update Homepage
    + point Vcs-* fields to salsa
    + drop ancient and unneeded (build) dependencies
    + add new build dependencies required by the new upstream version
  * Remove -w from shebang to prevent breakage when libraries throw warnings.
    (Closes: #584728)
  * Restore inc directory in overridden clean target, in case a test fails.
  * Bump debhelper compat/dependency to 11.
  * Bump Standards-Version to 4.1.4.
  * Convert debian/copyright to machine-readable format.
  * Add upstream metadata file.
  * Point watch file to CPAN.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 03 Jun 2018 17:23:48 +0200

grepmail (5.3033-8) unstable; urgency=medium

  * QA upload.
  * debian/control:
     Bumped Standards-Version to 3.9.7.
     Updated the Vcs-* to use https.
  * debian/watch: removed an old extra source.

 -- Gilmar dos Reis Queiroz <gilmardosreislpi@gmail.com>  Sat, 27 Feb 2016 18:15:35 -0300

grepmail (5.3033-7) unstable; urgency=medium

  * QA upload.
  * New upstream homepage.
  * Migrated DH level to 9.
  * debian/control:
     - bumped Standards-Version to 3.9.6.
     - updated vcs-browser to the cgit standard.
  * debian/copyright:
     - migrated to 1.0 format.
     - updated all data.
     - added Upstream-Contact field in header.
  * debian/watch:
     - improved.
     - updated version to 3.

 -- Gilmar dos Reis Queiroz <gilmardosreislpi@gmail.com>  Mon, 11 Jan 2016 14:25:11 -0200

grepmail (5.3033-6) unstable; urgency=low

  * QA upload.
  * Move away directory with bundled libs for test suite. Thanks to brian
    m. carlson for the analysis. (Closes: #719963)
  * Make URLs in Vcs-* fields canonical.
  * Declare compliance with Debian Policy 3.9.4.

 -- gregor herrmann <gregoa@debian.org>  Tue, 20 Aug 2013 17:21:35 +0200

grepmail (5.3033-5) unstable; urgency=low

  * QA upload.
  * Update Standards-Version to 3.8.4 (No changes needed)
  * debian/patches/man_page.patch: Added
  * Switch to 3.0 (quilt) source format: Add debian/source/format
  * debian/patches/midnight.patch: Added, thanks to Manuel Prinz
    (Closes: #559588)
  * debian/patches/fix_nonexistent_mailbox_test.patch: Fix FTBFS with
    libmail-mbox-messageparser-perl 1.5002 (Closes: #549782)
  * Update libmail-mbox-messageparser-perl build dependency and Depends to
    1.5002 in debian/control
  * debian/copyright: Refer to GPL-2 explicitly
  * Update Vcs-git and Vcs-Browser fields to Alioth

 -- Vincent Legout <vincent@legout.info>  Wed, 28 Apr 2010 19:04:54 +0800

grepmail (5.3033-4) unstable; urgency=low

  * debhelper v7; 3 line rules file
  * New format copyright file.
  * Orphaned the package.

 -- Joey Hess <joeyh@debian.org>  Fri, 25 Apr 2008 18:24:43 -0400

grepmail (5.3033-3) unstable; urgency=low

  * Add Homepage field.

 -- Joey Hess <joeyh@debian.org>  Mon, 10 Mar 2008 16:01:49 -0400

grepmail (5.3033-2) unstable; urgency=low

  * Fix man page year typo. Closes: #428973
  * Fix .orig.tar.gz

 -- Joey Hess <joeyh@debian.org>  Sat, 08 Sep 2007 21:54:11 -0400

grepmail (5.3033-1) unstable; urgency=low

  * New upstream release.
  * Improve synopsis. Closes: #400618
  * Current standards-version.

 -- Joey Hess <joeyh@debian.org>  Tue,  6 Mar 2007 15:05:03 -0500

grepmail (5.3032-2) unstable; urgency=low

  * Current policy.
  * Build-Depends/Build-Depends-Indep split.

 -- Joey Hess <joeyh@debian.org>  Sun, 18 Dec 2005 17:22:05 -0500

grepmail (5.3032-1) unstable; urgency=low

  * Switch watch file to using sf redirector.
  * New upstream release.
  * Depend and build depend on most recent messageparser.
  * Drop old dependency on libinline-perl.

 -- Joey Hess <joeyh@debian.org>  Thu,  4 Aug 2005 11:39:28 -0400

grepmail (5.3031-1) unstable; urgency=low

  * New upstream release.
  * Build-depend on newest messageparser so new test script succeeds.

 -- Joey Hess <joeyh@debian.org>  Sat, 11 Jun 2005 22:04:00 -0400

grepmail (5.3030-3) unstable; urgency=low

  * Force TZ=UTC during build to avoid test suite failure with other
    timezone settings. Closes: #302703

 -- Joey Hess <joeyh@debian.org>  Sat,  2 Apr 2005 15:53:59 -0500

grepmail (5.3030-2) unstable; urgency=low

  * Add missing build dep on libdate-manip-perl. Closes: #302569

 -- Joey Hess <joeyh@debian.org>  Fri,  1 Apr 2005 07:43:45 -1000

grepmail (5.3030-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Wed, 19 Jan 2005 22:03:23 -0500

grepmail (5.30.1-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Fri,  1 Oct 2004 05:15:33 -0400

grepmail (5.30-1) unstable; urgency=low

  * New upstream release.
  * Remove recursive.t hack, fixed upstream.
  * Depends and build-depends on 1.20 of the messageparser.

 -- Joey Hess <joeyh@debian.org>  Wed, 14 Jul 2004 11:01:28 -0400

grepmail (5.23-3) unstable; urgency=low

  * Add dependency on libscalar-list-utils-perl, to support pre perl 5.8
    systems. Closes: #244976

 -- Joey Hess <joeyh@debian.org>  Tue, 20 Apr 2004 19:25:49 -0400

grepmail (5.23-2) unstable; urgency=low

  * Add a build dep on bzip2, so the tests that exercise that work.
    Closes: #241956

 -- Joey Hess <joeyh@debian.org>  Sat,  3 Apr 2004 19:58:15 -0500

grepmail (5.23-1) unstable; urgency=low

  * New upstream release.
  * Hack recursive.t to work if grepmail is checked into subversion.

 -- Joey Hess <joeyh@debian.org>  Mon,  9 Feb 2004 13:57:03 -0500

grepmail (5.22-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Mon, 27 Oct 2003 12:19:27 -0500

grepmail (5.21-2) unstable; urgency=low

  * Move from build-depends-indep to build-depends, to meet current policy.

 -- Joey Hess <joeyh@debian.org>  Wed,  3 Sep 2003 11:58:15 -0400

grepmail (5.21-1) unstable; urgency=low

  * New upstream release.
    - Fixed searching of $HOME/Mail. Closes: #204258
    - Apparently fixes hang grepping uncompressed mailbox. Closes: #204884
  * Remove make test buglet workaround in debian/rules.
  * Depends on new libmail-mbox-messageparser-perl 1.11.
  * Also added build-depends on that.

 -- Joey Hess <joeyh@debian.org>  Thu, 28 Aug 2003 15:08:39 -0400

grepmail (5.10-2) unstable; urgency=low

  * Convert huge regexp lines in POD docs to verbatim paragraphs, so they are
    not hyphenated which is very confusing and made groff output ugly
    warnings. (See bug #199099)

 -- Joey Hess <joeyh@debian.org>  Sat, 16 Aug 2003 12:55:44 -0400

grepmail (5.10-1) unstable; urgency=low

  * New upstream release. Mail::Mbox::MessageParser is split into a CPAN perl
    module.

 -- Joey Hess <joeyh@debian.org>  Thu,  7 Aug 2003 17:49:28 -0400

grepmail (5.00-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Fri, 11 Jul 2003 14:33:45 -0400

grepmail (4.91-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Sun,  2 Feb 2003 15:59:59 -0500

grepmail (4.90-2) unstable; urgency=low

  * Don't call SETUP_CACHE if $USE_CACHING is false. Closes: #173799

 -- Joey Hess <joeyh@debian.org>  Wed, 25 Dec 2002 21:23:08 -0500

grepmail (4.90-1) unstable; urgency=low

  * New upstream release. Removes FastReader since plain perl implementation
    is now as fast. So:
    - remove build-dep on libinline-perl
    - package is now arch all

 -- Joey Hess <joeyh@debian.org>  Wed, 27 Nov 2002 22:59:25 -0500

grepmail (4.80-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Sun, 13 Oct 2002 19:51:29 -0400

grepmail (4.72-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Tue,  3 Sep 2002 14:40:38 -0400

grepmail (4.71-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Thu,  1 Aug 2002 22:30:34 -0400

grepmail (4.70-4) unstable; urgency=low

  * Build for perl 5.8.
  * Modified regexps, test suite, etc to work with perl 5.8.

 -- Joey Hess <joeyh@debian.org>  Thu,  1 Aug 2002 12:29:00 -0400

grepmail (4.70-3) unstable; urgency=low

  * Don't use dh_installmanpages.

 -- Joey Hess <joeyh@debian.org>  Thu, 13 Jun 2002 18:28:33 -0400

grepmail (4.70-2) unstable; urgency=low

  * Debhelper v4.

 -- Joey Hess <joeyh@debian.org>  Sat,  1 Jun 2002 18:24:30 -0400

grepmail (4.70-1) unstable; urgency=low

  * New upstream, Closes: #125941
  * Included the new anonymize_mailbox program as an example, it's not
    quite ready to be put in bin yet.
  * Use debhelper v3.

 -- Joey Hess <joeyh@debian.org>  Thu,  3 Jan 2002 01:24:32 -0500

grepmail (4.60-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Sun, 21 Oct 2001 02:37:47 -0400

grepmail (4.51-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Sun,  9 Sep 2001 19:56:33 -0400

grepmail (4.50-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Fri,  3 Aug 2001 10:38:55 -0400

grepmail (4.48-2) unstable; urgency=low

  * No changes. Oddly, this loses the suggests on libinline-perl. So how
    come it was in the previous version? Damned if I know. Closes: #101290

 -- Joey Hess <joeyh@debian.org>  Mon, 18 Jun 2001 13:43:00 -0400

grepmail (4.48-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Wed,  6 Jun 2001 15:20:38 -0400

grepmail (4.47-2) unstable; urgency=low

  * Moved out of privlib, Closes: #95516

 -- Joey Hess <joeyh@debian.org>  Sun, 29 Apr 2001 22:14:30 -0400

grepmail (4.47-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Thu, 29 Mar 2001 15:17:10 -0800

grepmail (4.46-2) unstable; urgency=low

  * Build deps are no longer -indep, Closes: #85245

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Feb 2001 13:58:25 -0800

grepmail (4.46-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Sat, 24 Feb 2001 21:23:51 -0800

grepmail (4.45-3) unstable; urgency=low

  * Corrected build dependencies, Closes: #85245

 -- Joey Hess <joeyh@debian.org>  Thu,  8 Feb 2001 09:39:18 -0800

grepmail (4.45-2) unstable; urgency=low

  * Enabled the FastReader stuff, which speeds up grepmail by 10 to 50%.
    This makes the package arch: any not all, and requires tighter perl
    dependencies.
  * Reupload, damn dpkg-dev.

 -- Joey Hess <joeyh@debian.org>  Wed, 17 Jan 2001 13:05:27 -0800

grepmail (4.45-1) unstable; urgency=low

  * New upstream release. Fixes netscape problem, Closes: #79328
    Not yet including Mail::Folder::FastReader as it is is experimental;
    hacked Makefile.PL to not prompt.

 -- Joey Hess <joeyh@debian.org>  Tue, 16 Jan 2001 15:27:03 -0800

grepmail (4.43-1) unstable; urgency=low

  * New upstream release.
  * Use debhelper v2.

 -- Joey Hess <joeyh@debian.org>  Mon, 25 Sep 2000 15:16:08 -0700

grepmail (4.42-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Sun, 20 Aug 2000 17:19:41 -0700

grepmail (4.41-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Wed, 14 Jun 2000 11:38:30 -0700

grepmail (4.40-1) unstable; urgency=low

  * New upstream.
  * Build and dependency changes.

 -- Joey Hess <joeyh@debian.org>  Mon, 15 May 2000 16:03:03 -0700

grepmail (4.31-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Tue,  2 May 2000 23:40:43 -0700

grepmail (4.30-1) unstable; urgency=low

  * New upstream.
  * Updated url in copyright file.

 -- Joey Hess <joeyh@debian.org>  Tue,  2 May 2000 16:25:57 -0700

grepmail (4.23-3) unstable; urgency=low

  * New upstream. Reupload with full source.

 -- Joey Hess <joeyh@debian.org>  Sun, 16 Apr 2000 16:48:22 -0700

grepmail (4.23-2) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Thu, 13 Apr 2000 21:44:01 -0700

grepmail (4.1-1) unstable; urgency=low

  * New upstream.

 -- Joey Hess <joeyh@debian.org>  Tue, 11 Jan 2000 16:07:30 -0800

grepmail (3.9-4) unstable; urgency=low

  * Build dep.

 -- Joey Hess <joeyh@debian.org>  Sat,  4 Dec 1999 01:01:14 -0800

grepmail (3.9-3) unstable; urgency=low

  * Removed install-stamp stuff, which can cause obscure problems.
  * Reuploaded full source, which I messed up last time.

 -- Joey Hess <joeyh@debian.org>  Thu, 30 Sep 1999 13:01:07 -0700

grepmail (3.9-2) unstable; urgency=low

  * Modified grepmail so it doesn't use the sh-invokes-perl trick. This was
    breaking for someone who uses tcsh as their shell. I don't know why.
    (Closes: #45116)

 -- Joey Hess <joeyh@debian.org>  Wed, 15 Sep 1999 13:06:18 -0700

grepmail (3.9-1) unstable; urgency=low

  * New upstream version. Now available in two flavors, using Date::Manip or
    Date::Parse. Linked the Manip flavor to grepmail, since that is what
    it's used in the past.
  * FHS.

 -- Joey Hess <joeyh@debian.org>  Fri, 10 Sep 1999 21:36:19 -0700

grepmail (3.6-3) unstable; urgency=low

  * Now depends on perl5 | perl, I'll kill the | perl bit later on, but it
    seems to make sense for the transition.

 -- Joey Hess <joeyh@debian.org>  Sun,  4 Jul 1999 10:58:38 -0700

grepmail (3.6-2) unstable; urgency=low

  * Made CleanExit print errors to stderr. Patch sent upstream.

 -- Joey Hess <joeyh@debian.org>  Fri, 11 Jun 1999 14:37:09 -0700

grepmail (3.6-1) unstable; urgency=low

  * New upstream release that incorporates my security patch.

 -- Joey Hess <joeyh@debian.org>  Mon, 22 Feb 1999 16:21:13 -0800

grepmail (3.5-1) unstable; urgency=low

  * New upstream release that fixes the tmpfile problem, but not the race
    conditions. Re-applied my security patch.
  * Fixed author's home page address on man page.

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Feb 1999 23:40:24 -0800

grepmail (3.4.1-3) frozen unstable; urgency=high

  * Fixed multiple file in /tmp security holes that could delete data.
  * Fixed multiple race conditions that could make private data public.
  * Patch sent upstream.

 -- Joey Hess <joeyh@debian.org>  Tue,  9 Feb 1999 21:08:15 -0800

grepmail (3.4.1-2) unstable; urgency=low

  * Fixed perl path.

 -- Joey Hess <joeyh@debian.org>  Fri, 22 Jan 1999 12:05:11 -0800

grepmail (3.4.1-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Thu, 21 Jan 1999 21:09:31 -0800

grepmail (3.4-2) unstable; urgency=low

  * Now suggests: libdate-manip-perl.

 -- Joey Hess <joeyh@debian.org>  Wed, 28 Oct 1998 14:33:51 -0800

grepmail (3.4-1) unstable; urgency=low

  * New upstream release.

 -- Joey Hess <joeyh@debian.org>  Mon, 26 Oct 1998 14:33:11 -0800

grepmail (3.3-1) unstable; urgency=low

  * First release.

 -- Joey Hess <joeyh@debian.org>  Mon, 19 Oct 1998 20:33:10 -0700
